
-- | This program generates the ci.yml file used for GitLab builds.
module Main (main) where

import Data.Version (Version, makeVersion, showVersion)
import Data.String (fromString)
import Data.Time.Calendar (Day, fromGregorian)
import Text.Megaparsec (Parsec)
import Text.Megaparsec qualified as Parser
import Text.Megaparsec.Char.Lexer qualified as Parser
import Data.Text (Text)
import Data.Void (Void)
import Data.Aeson (FromJSON, ToJSON, (.=))
import Data.Aeson qualified as JSON
import Data.Aeson.Key qualified as Key
import Data.Aeson.KeyMap qualified as KeyMap
import Data.Yaml qualified as Yaml
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Map (Map)
import Data.Map qualified as Map
import Network.HTTP.Simple (parseRequest, httpJSON, getResponseBody)
import System.Directory (createDirectoryIfMissing)
import Data.ByteString qualified as ByteString

type Parser = Parsec Void Text

data Snapshot =
    LTS Version
  | Nightly Day
    deriving (Eq, Ord)

instance Show Snapshot where
  show (LTS v) = "lts-" ++ showVersion v
  show (Nightly d) = "nightly-" ++ show d

versionParser :: Parser Version
versionParser = do
  v1 <- Parser.decimal
  _ <- Parser.single '.'
  v2 <- Parser.decimal
  pure $ makeVersion [v1,v2]

dayParser :: Parser Day
dayParser = do
  y <- Parser.decimal
  _ <- Parser.single '-'
  m <- Parser.decimal
  _ <- Parser.single '-'
  d <- Parser.decimal
  pure $ fromGregorian y m d

snapshotParser :: Parser Snapshot
snapshotParser = Parser.choice
  [ Parser.chunk "lts-" >> LTS <$> versionParser
  , Parser.chunk "nightly-" >> Nightly <$> dayParser
    ]

instance FromJSON Snapshot where
  parseJSON = JSON.withText "Snapshot" $ \t ->
    either (fail . Parser.errorBundlePretty) pure $
      Parser.runParser snapshotParser "JSON" t

instance ToJSON Snapshot where
  toJSON = JSON.String . fromString . show

downloadSnapshots :: IO (Set Snapshot)
downloadSnapshots = do
  req <- parseRequest "https://www.stackage.org/download/snapshots.json"
  resp <- httpJSON req
  pure $ Set.fromList $ Map.elems (getResponseBody resp :: Map Text Snapshot)

insertJSONValue
  :: ToJSON a
  => [Text] -- ^ Selector
  -> a -- ^ Value to insert
  -> JSON.Value -- ^ Original value
  -> JSON.Value -- ^ Modified value
insertJSONValue [] v _ = JSON.toJSON v
insertJSONValue (t:ts) v (JSON.Object o) = JSON.Object $
  let k = Key.fromText t
  in  case KeyMap.lookup k o of
        Just input -> KeyMap.insert k (insertJSONValue ts v input) o
        _ -> KeyMap.insert k (insertJSONValue ts v $ JSON.Object KeyMap.empty) o
insertJSONValue _ _ input = input

main :: IO ()
main = do
  putStrLn "generate-ci"
  snapshotSet <- downloadSnapshots
  -- Here it's decided what Stackage snapshots to use.
  let selectedSnapshots = reverse $ take 5 $ Set.toDescList snapshotSet
  ----
  template <- Yaml.decodeFileThrow "ci-template.yml"
  let template' = insertJSONValue
                    ["build","parallel","matrix"]
                    [JSON.object ["snapshot" .= selectedSnapshots]]
                $ template
  createDirectoryIfMissing False "public"
  ByteString.writeFile "public/ci.yml" $
    "# File generated from https://gitlab.com/daniel-casanueva/haskell-common/-/blob/main/ci-template.yml\n" <> Yaml.encode template'
